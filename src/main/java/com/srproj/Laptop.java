package com.srproj;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Laptop {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "model", nullable = true)
	private String model;
	@Column(name = "brand", nullable = true)
	private String brand;
	@Column(name = "release_date", nullable = true)
	private String releaseDate;
	@Column(name = "cpu", nullable = true)
	private String cpu;
	@Column(name = "ram", nullable = true)
	private String ram;
	@Column(name = "gpu", nullable = true)
	private String gpu;
	
	public Laptop() {
		super();
		}
	
	public Laptop(Integer id, String model, String brand, String releaseDate, String cpu, String ram, String gpu) {
		super();
		this.id = id;
		this.model = model;
		this.brand = brand;
		this.releaseDate = releaseDate;
		this.cpu = cpu;
		this.ram = ram;
		this.gpu = gpu;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getCpu() {
		return cpu;
	}
	public void setCpu(String cpu) {
		this.cpu = cpu;
	}
	public String getRam() {
		return ram;
	}
	public void setRam(String ram) {
		this.ram = ram;
	}
	public String getGpu() {
		return gpu;
	}
	public void setGpu(String gpu) {
		this.gpu = gpu;
	}
	

}
