package com.srproj;

import java.util.List;

import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
@Transactional
public class LaptopService {
 
    @Autowired
    private LaptopRepository repo;
     
    public List<Laptop> listAll() {
        return repo.findAll();
    }
     
    public void save(Laptop product) {
        repo.save(product);
    }
     
    public Laptop get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
}